
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "artez_sdk/version"

Gem::Specification.new do |spec|
  spec.name          = "artez_sdk"
  spec.version       = ArtezSdk::VERSION
  spec.authors       = ["Alberto Mota"]
  spec.email         = ["alberto@thejibe.com"]

  spec.summary       = %q{Artez SDK.}
  spec.description   = %q{Basic Artez SDK to allow registering for events.}
  spec.homepage      = "https://bitbucket.org/thejibe/artezsdk.git"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "pry", "~> 0.12.2"
  spec.add_development_dependency "pry-coolline"
  spec.add_development_dependency "guard-rspec"
  spec.add_development_dependency "factory_bot_rails"
  spec.add_development_dependency "faker"

  spec.add_dependency 'httparty', '~> 0.16.2'

end
