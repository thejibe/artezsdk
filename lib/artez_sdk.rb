require "artez_sdk/version"
require 'httparty'

module ArtezSdk
  
  class ArtezSDKError < StandardError; end
  class ArtezSDKInvalidParameterError < StandardError; end

  class Client
    include HTTParty
  
    base_uri "https://secure.e2rm.com/api"
    format :json
    default_params :output => 'json'
  
    attr_accessor :headers, :location_id, :registration_type_id, :event_id
  
    def initialize(access_token, location_id, registration_type_id, event_id)
      self.location_id = location_id
      self.registration_type_id = registration_type_id
      self.event_id = event_id
      self.headers = { Authorization: "Basic #{access_token}"}
    end

    # Returns details about a constituent
    def constituent(id)
      response = get("/Constituents?ConstituentID=#{id}")
      handle_response response
    end
  
    # Checks if username is in use
    def username_in_use?(username)
      response = get("/Logins?username=#{username}")
      return true if (response.success?)
      false
    end

    # Creates a constituent
    def create_constituent(payload)
      response = post("/Constituents", payload)
      handle_response response
    end

    # Gets details for a constituent
    def get_constituent(constituent_id)
      response = get("/Constituents?ConstituentID=#{constituent_id}")
      handle_response response
    end

    # Creates a transaction
    def create_transaction(payload)
      response = post("/Transactions", payload)
      handle_response response
    end

    # Updates a transaction
    def update_transaction(transaction_id, payload)
      response = put("/Transactions/#{transaction_id}", payload)
      handle_response response
    end

    # Creates a registration
    def create_registration(payload)
      response = post("/Registrations", payload)
      handle_response response
    end

    # Creates a registration
    def get_registration(first_name, last_name)
      response = get("/Registrations?eventid=#{self.event_id}&firstname=#{first_name}&lastname=#{last_name}")
      handle_response response
    end

    # Forgot password
    def forgot_password(payload)
      response = post("/ForgotPassword", payload)
      handle_response response
    end

    private

    def get(url)
      self.class.get(url, headers: self.headers)
    end
  
    def post(url, payload)
      self.class.post(url, body: payload, headers: self.headers)
    end
  
    def put(url, payload)
      self.class.put(url, body: payload, headers: self.headers)
    end
  
    def handle_response(response)
      if response.success?
        response
      else
        raise ArtezSdk::ArtezSDKInvalidParameterError, response
      end
    end
  
  end
  

end

