RSpec.describe ArtezSdk do
  before(:each) do
    @client = ArtezSdk::Client.new(
      "QkNDSEY6OTg1Yzc0NTEwN2NiNGFlOWFmNDE1MjVjNDQ2MGFkMjM=",
      279058,
      80972,
      279057
    )
  end

  it "has a version number" do
    expect(ArtezSdk::VERSION).not_to be nil
  end

  it "checks username is not in use correctly" do
    expect(@client.username_in_use?("testusernamethatdoesntexist")).to eq(false)
  end

  it "checks username is in use correctly" do
    expect(@client.username_in_use?("grousetest101")).to eq(true)
  end

  it "creates new constituents" do
    payload = build(:constituent).to_h
    response = @client.create_constituent(payload).parsed_response
    expect(response.key?("ConstituentID")).to eq(true)
  end

  it "gets information for a constituent" do
    payload = build(:constituent).to_h
    constituent_id = @client.create_constituent(payload).parsed_response["ConstituentID"]
    response = @client.get_constituent(constituent_id).parsed_response
    expect(response["EmailAddress"]).to eq(payload[:EmailAddress])
  end

  it "creates new transactions" do
    constituent_id = @client.create_constituent(build(:constituent).to_h).parsed_response["ConstituentID"]
    payload = build(:transaction, ConstituentID: constituent_id).to_h
    response = @client.create_transaction(payload).parsed_response
    expect(response.key?("TransactionID")).to eq(true)
  end

  it "creates new registrations" do
    constituent_id = @client.create_constituent(build(:constituent).to_h).parsed_response["ConstituentID"]
    transaction_id = @client.create_transaction(build(:transaction, ConstituentID: constituent_id).to_h).parsed_response["TransactionID"]
    payload = 
      build(
        :registration, 
        ConstituentID: constituent_id,
        TransactionID: transaction_id,
        LocationID: @client.location_id,
        RegistrationTypeID: @client.registration_type_id
      ).to_h

    response = @client.create_registration(payload).parsed_response
    expect(response.key?("RegistrationID")).to eq(true)
  end

  it "gets registration info for a user and event" do
    registrations = @client.get_registration("Chris", "Sweeting").parsed_response
    expect(registrations).not_to be_empty
  end

  it "updates transactions" do
    constituent_id = @client.create_constituent(build(:constituent).to_h).parsed_response["ConstituentID"]
    payload = build(:transaction, ConstituentID: constituent_id).to_h
    transaction_id = @client.create_transaction(payload).parsed_response["TransactionID"]
    response = @client.update_transaction(transaction_id, build(:transaction_update).to_h)
    expect(response.code).to eq(200)
  end

  it "triggers forgot password" do
    payload = build(:forgot_password, EventID: @client.event_id, UserName: "grousetest101").to_h
    expect(@client.forgot_password(payload).code).to eq(200)
  end


end
