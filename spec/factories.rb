FactoryBot.define do

    factory :constituent, class: OpenStruct do
        OrganizationName {nil}
        FirstName { Faker::Name.first_name }
        LastName { Faker::Name.last_name }
        EmailAddress { Faker::Internet.safe_email }
        PhoneNumber {"555-111-3334"}
        LanguagePreference {"en-CA"}
        IsOrganization {false}
        AllowContactViaPost {false} 
        AllowContactViaEmail {true} 
        TitleID {nil}

        skip_create
    end

    factory :registration, class: OpenStruct do
        Username { Faker::Internet.username }
        ConstituentID { 0000 }
        Password { Faker::Internet.password(5, 20) } 
        LocationID { 0000 }
        RegistrationTypeID { 0000 }
        TeamID { nil }
        CorporateTeamId { nil }
        SearchConsent { false } 
        ScoreboardConsent { false }
        WaiveRegistrationFee { false }
        TransactionID  { 0000 } 

        skip_create
    end

    factory :transaction, class: OpenStruct do
        ConstituentID { 0000 }

        skip_create
    end

    factory :transaction_update, class: OpenStruct do
        Status { "Succeeded" }

        skip_create
    end

    factory :forgot_password, class: OpenStruct do
        EventID { 0000 }
        UserName { Faker::Internet.username }

        skip_create
    end

  end